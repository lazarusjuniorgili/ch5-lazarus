function dapatkanPilihanComputer() {
  const comp = Math.random();
  if (comp < 0.34) return "batu";
  if (comp >= 0.34 && comp < 0.67) return "gunting";
  return "kertas";
}

function dapatkanHasil(comp, pemain) {
  if (pemain == comp) return "seri";
  if (pemain == "batu") return comp == "gunting" ? "Menang" : "Kalah";
  if (pemain == "gunting") return comp == "batu" ? "Kalah" : "Menang";
  if (pemain == "kertas") return comp == "gunting" ? "Kalah" : "Menang";
}

const pilihBatu = document.querySelector(".batu");
pilihBatu.addEventListener("click", function () {
  const pilihanComputer = dapatkanPilihanComputer();
  const pilihanPemain = pilihBatu.className;
  const hasil = dapatkanHasil(pilihanComputer, pilihanPemain);

  const gambarComputer = document.querySelector(".gambar-computer-batu");
  gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

  const info = document.querySelector(".info");
  info.innerHTML = hasil;
});

const pilihGunting = document.querySelector(".gunting");
pilihGunting.addEventListener("click", function () {
  const pilihanComputer = dapatkanPilihanComputer();
  const pilihanPemain = pilihGunting.className;
  const hasil = dapatkanHasil(pilihanComputer, pilihanPemain);

  const gambarComputer = document.querySelector(".gambar-computer-gunting");
  gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

  const info = document.querySelector(".info");
  info.innerHTML = hasil;
});

const pilihKertas = document.querySelector(".kertas");
pilihKertas.addEventListener("click", function () {
  const pilihanComputer = dapatkanPilihanComputer();
  const pilihanPemain = pilihKertas.className;
  const hasil = dapatkanHasil(pilihanComputer, pilihanPemain);

  const gambarComputer = document.querySelector(".gambar-computer-kertas");
  gambarComputer.setAttribute("src", "assets/" + pilihanComputer + ".png");

  const info = document.querySelector(".info");
  info.innerHTML = hasil;
});

const reset = document.querySelector(".refresh");
reset.addEventListener("click", function () {
  window.location.reload();
});
