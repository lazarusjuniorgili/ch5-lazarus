// if (process.env.NODE_ENV !== "production") {
//   require("dotenvnv")
// }

// const express = require("express");
// const app = express();
// const PORT = 8000;
// const data = require("./login.json");
// const bcrypt = require("bcrypt");
// const fs = require("fs");
// const passport = require("passport");
// const flash = require("express-flash");
// const session = require("express-session");

// const initializePassport = require("./passport-config");
// const { use } = require("passport");
// initializePassport(passport, (email) => data.find((user) => user.email === email));

// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(flash());
// app.use(session({
//     secret: process.env.SESSION_SECRET,
//     resave: false,
//     saveUninitialized: false
//   })
//   app.use(passport.initialize())
//   app.use(passport.session())
// );
// app.set("view engine", "ejs");
// app.use(express.static("./public"));

// const validation = (reg, res, next) => {
//   const { email, password } = reg.body;
//   const isFind = login.find((row) => row.email == email && row.password == password);
//   if (isFind) {
//     res.status(400).json({ message: "Id Not Available" });
//   } else {
//     res.redirect("/games");
//   }
// };

// app.post("/login", (reg, res) => {
//   const { email, password } = reg.body;
//   const isFind = login.find((row) => row.email == email && row.password == password);
//   if (isFind) {
//     res.status(400).json({ message: "Invalid Username" });
//   } else {
//     res.redirect("/games");
//   }
// });

// app.use(validation);
// app.use("/games", login);

// app.post("/login", (reg, res) => {
//   if (reg.body.email == login.email && reg.body.password == login.password) {
//     // reg.session.user = reg.body.email;
//     res.redirect("/games");
//   } else {
//     res.end("Invalid Username");
//   }
// });

// app.get("/login", (reg, res) => {
//   res.render("login.ejs");
// });

// app.post("/login", passport.authenticate("local", {
//   successMessage: "/games",
//   failureRedirect: "/login",
//   failureFlash
// }))

// app.get("/register", (reg, res) => {
//   res.render("register.ejs");
// });

// app.post("/register", async (reg, res) => {
//   try {
//     const hashedPassword = await bcrypt.hash(reg.body.password, 10);
//     data.push({
//       id: Date.now().toString(),
//       name: reg.body.name,
//       email: reg.body.email,
//       password: hashedPassword,
//     });
//     res.redirect("/login");
//   } catch {
//     res.redirect("/register");
//   }
//   fs.writeFileSync("./login.json", JSON.stringify(data));
// });

// app.get("/", (reg, res) => {
//   res.render("index.ejs");
// });

// app.get("/home", (reg, res) => {
//   res.render("index.ejs");
// });

// app.get("/games", (reg, res) => {
//   res.render("games.ejs");
// });

// app.listen(PORT, () => console.log(`Listening at http://localhost:${PORT}`));

// -----------------------------------------------------------------------------------------------------

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const fs = require("fs");
const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const passport = require("passport");
const flash = require("express-flash");
const session = require("express-session");
const data = require("./login.json");
const PORT = 8000;

const initializePassport = require("./passport-config");
initializePassport(
  passport,
  (email) => data.find((user) => user.email === email),
  (id) => data.find((user) => user.id === id)
);

app.set("view-engine", "ejs");
app.use(express.static("./public"));
app.use(express.urlencoded({ extended: false }));
app.use(flash());
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.get("/", checkAuthenticated, (req, res) => {
  res.render("index.ejs", { name: req.user.name });
});

app.get("/login", checkNotAuthenticated, (req, res) => {
  res.render("login.ejs");
});

app.get("/games", (reg, res) => {
  res.render("games.ejs");
});

app.post(
  "/login",
  checkNotAuthenticated,
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  })
);

app.get("/register", checkNotAuthenticated, (req, res) => {
  res.render("register.ejs");
});

app.post("/register", checkNotAuthenticated, async (req, res) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    data.push({
      id: Date.now().toString(),
      name: req.body.name,
      email: req.body.email,
      password: hashedPassword,
    });
    res.redirect("/login");
  } catch {
    res.redirect("/register");
  }
  fs.writeFileSync("./login.json", JSON.stringify(data));
});

app.delete("/logout", (req, res) => {
  req.logOut();
  res.redirect("/login");
});

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect("/login");
}

function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }
  next();
}

app.listen(PORT, () => console.log(`Listening at http://localhost:${PORT}`));
